Management commands
===================


Create template
---------------

.. code:: bash

    (venv)$ python manage.py emailhub --create-template


List templates
--------------

.. code:: bash

    (venv)$ python manage.py emailhub --list-templates


Send
----

Send unsent emails:

.. code:: bash

    (venv)$ python manage.py emailhub --send


Since email batch sending is throttled, you can set up a cron job to run every
minutes to send unsent emails. This way an email will never wait more than one
minute before being sent in a optimal situation.

.. code:: bash

    *  * * * * root /venv/path/bin/python /project/path/manage.py emailhub --send >> /var/log/emailhub.log 2>&1



Send test
---------

Sends an email test, accepts a destination email address or a user ID.

.. code:: bash

    (venv)$ python manage.py emailhub --send-test bob@test.com
    Content-Type: text/plain; charset="utf-8"
    MIME-Version: 1.0
    Content-Transfer-Encoding: 7bit
    Subject: Test email
    From: no-reply@domain.com
    To: bob@test.com
    Date: Tue, 06 Mar 2018 19:01:16 -0000
    Message-ID: <20180306190116.2915.53062@singularity>

    This is a test.
    -------------------------------------------------------------------------------


Status
------

.. code:: bash

    (venv)$ python manage.py emailhub --status

	Unsent                        14
	Drafts                        7
	Is sent                       7
	Errors                        0
