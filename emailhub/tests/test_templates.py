# -*- coding: utf-8 -*-
"""
EmailHub email backends tests
"""
from django.test import TestCase
from django.contrib.auth import get_user_model

from emailhub.conf import settings as emailhub_settings
from emailhub.models import EmailTemplate
from emailhub.utils.email import EmailFromTemplate, send_unsent_emails

User = get_user_model()

TEST_EMAIL = {
    'slug': 'test-template',
    'subject': 'Subject here!',
    'is_auto_send': True,
}


class EmailTemplateAutoSendTestCase(TestCase):
    """
    Test that email are send according to configuration rules
    """

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(
            username='testuser',
            email='test@user.com',
            is_active=True)

    def test_force_auto_send_off(self):
            TEST_EMAIL['is_auto_send'] = False
            EmailTemplate.objects.create(**TEST_EMAIL)
            msg = EmailFromTemplate('test-template').send_to(
                self.user, force=True)
            self.assertEqual(msg.state, 'pending')
            self.assertEqual(msg.is_draft, False)
            self.assertEqual(msg.is_pending, True)
            self.assertEqual(msg.is_locked, False)
            self.assertEqual(msg.is_error, False)
            self.assertEqual(msg.is_sent, False)
            self.assertEqual(msg.from_template, 'test-template')

    def test_force_auto_send_on(self):
            TEST_EMAIL['is_auto_send'] = True
            EmailTemplate.objects.create(**TEST_EMAIL)
            msg = EmailFromTemplate('test-template').send_to(
                self.user, force=True)
            self.assertEqual(msg.state, 'pending')
            self.assertEqual(msg.is_draft, False)
            self.assertEqual(msg.is_pending, True)
            self.assertEqual(msg.is_locked, False)
            self.assertEqual(msg.is_error, False)
            self.assertEqual(msg.is_sent, False)
            self.assertEqual(msg.from_template, 'test-template')

    def test_auto_send_off(self):
            TEST_EMAIL['is_auto_send'] = False
            EmailTemplate.objects.create(**TEST_EMAIL)
            msg = EmailFromTemplate('test-template').send_to(self.user)
            send_unsent_emails()
            self.assertEqual(msg.state, 'draft')
            self.assertEqual(msg.is_draft, True)
            self.assertEqual(msg.is_pending, False)
            self.assertEqual(msg.is_locked, False)
            self.assertEqual(msg.is_error, False)
            self.assertEqual(msg.is_sent, False)
            self.assertEqual(msg.from_template, 'test-template')

    def test_auto_send_on(self):
            TEST_EMAIL['is_auto_send'] = True
            EmailTemplate.objects.create(**TEST_EMAIL)
            msg = EmailFromTemplate('test-template').send_to(self.user)
            send_unsent_emails()
            self.assertEqual(msg.state, 'pending')
            self.assertEqual(msg.is_draft, False)
            self.assertEqual(msg.is_pending, True)
            self.assertEqual(msg.is_locked, False)
            self.assertEqual(msg.is_error, False)
            self.assertEqual(msg.is_sent, False)
            self.assertEqual(msg.from_template, 'test-template')


class EmailTemplateContextTestCase(TestCase):
    """
    Test that variables are correctly injected in templates
    """

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(
            username='testuser',
            email='test@user.com',
            is_active=True)

    def wrap_text(self, i):
        return emailhub_settings.TEXT_TEMPLATE.format(
            template_tags=' '.join(emailhub_settings.PRELOADED_TEMPLATE_TAGS),
            content=i)

    def wrap_html(self, i):
        return emailhub_settings.HTML_TEMPLATE.format(
            template_tags=' '.join(emailhub_settings.PRELOADED_TEMPLATE_TAGS),
            content=i)

    def test_context_user(self):
        with self.settings(EMAIL_BACKEND='emailhub.backends.smtp.EmailBackend'):
            TEST_EMAIL['text_content'] = "Hello {{ user }}"
            TEST_EMAIL['html_content'] = "Hello <b>{{ user }}</b>"
            EmailTemplate.objects.create(**TEST_EMAIL)
            msg = EmailFromTemplate('test-template').send_to(self.user)

            self.assertEqual(msg.body_text, self.wrap_text('Hello testuser'))
            self.assertEqual(msg.body_html,
                             self.wrap_html('Hello <b>testuser</b>'))

    def test_context_language(self):
        with self.settings(EMAIL_BACKEND='emailhub.backends.smtp.EmailBackend'):
            TEST_EMAIL['text_content'] = "Hello {{ language }}"
            TEST_EMAIL['html_content'] = "Hello <b>{{ language }}</b>"
            EmailTemplate.objects.create(**TEST_EMAIL)
            msg = EmailFromTemplate('test-template').send_to(self.user)

            self.assertEqual(msg.body_text, self.wrap_text('Hello en'))
            self.assertEqual(msg.body_html,
                             self.wrap_html('Hello <b>en</b>'))

    def test_context_email(self):
        with self.settings(EMAIL_BACKEND='emailhub.backends.smtp.EmailBackend'):
            TEST_EMAIL['text_content'] = "Hello {{ email.template_slug }}"
            TEST_EMAIL['html_content'] = \
                "Hello <b>{{ email.template_slug }}</b>"
            EmailTemplate.objects.create(**TEST_EMAIL)
            msg = EmailFromTemplate('test-template').send_to(self.user)

            self.assertEqual(msg.body_text,
                             self.wrap_text('Hello test-template'))
            self.assertEqual(msg.body_html,
                             self.wrap_html('Hello <b>test-template</b>'))

    def test_context_extra(self):
        with self.settings(EMAIL_BACKEND='emailhub.backends.smtp.EmailBackend'):
            TEST_EMAIL['text_content'] = "Hello {{ extra }}"
            TEST_EMAIL['html_content'] = "Hello <b>{{ extra }}</b>"
            EmailTemplate.objects.create(**TEST_EMAIL)
            msg = EmailFromTemplate('test-template', extra_context={
                'extra': 'TEST'}).send_to(self.user)

            self.assertEqual(msg.body_text, self.wrap_text('Hello TEST'))
            self.assertEqual(msg.body_html, self.wrap_html('Hello <b>TEST</b>'))
