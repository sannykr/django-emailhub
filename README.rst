django-emailhub
===============

.. image:: https://gitlab.com/h3/django-emailhub/badges/master/build.svg
    :target: https://gitlab.com/h3/django-emailhub/pipelines

.. image:: https://readthedocs.org/projects/django-emailhub/badge/?version=latest
    :target: https://django-emailhub.readthedocs.io/en/latest/?badge=latest


`Django EmailHub <http://django-emailhub.readthedocs.io/en/latest/>`_ is an
application that bring advanced email functionnalities to Django such as
templates, draft state, batch sending and archiving.

**Note**: This is a work in progress and in early development stage.


Supported Python/Django versions
--------------------------------

* django: **1.8, 1.9, 1.10, 1.11, 1.9**
* python: **2.7, 3.4, 3.5**, 3.6**

**Note**: Work in progress, expect backward incompatible changes.


Links
-----

* `EmailHub Documentation <http://django-emailhub.readthedocs.io/en/latest/>`_
